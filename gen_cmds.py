from subprocess import run
import sys

args = sys.argv

node = "{}".format(args[1])
filename = "{}".format(args[2])
file_id = "{}".format(args[3])
total_frames = "{}".format(args[4])
max_cmds = 50
if (len(args) > 5):
    max_cmds = int(args[5])

out = run(["bash", "check_frames.sh", file_id, total_frames], capture_output=True, text=True).stdout
frames = out.replace("> {}_".format(file_id), "").replace("_{}.part".format(total_frames), "").split()
frames = [ x for x in frames if ".part" not in x ]
frames = [ x for x in frames if "> " not in x ]
frames = [ x for x in frames if "<" not in x ]

if (not frames):
    print("Empty list. We have all the parts")
else:
    frames = [int(x) for x in frames]
    frames = sorted(frames)
    print("Missing frames:")
    print(frames)
    start_frame = []
    end_frame = []
    start_frame.append(frames[0])
    end_frame.append(frames[0] + 1)
    cmd_counter = 1
    for i in range(len(frames)):
        if (i != 0):
            if (frames[i] == frames[i-1] + 1):
                end_frame[-1] = frames[i] + 1
            else:
                end_frame.append(frames[i] + 1)
                start_frame.append(frames[i])
                cmd_counter += 1
    print("\nGenerating commands...")
    cmds = []
    t0 = 5
    for i, sf in enumerate(start_frame):
        dt = end_frame[i] - sf
        cmds.append("com_send_cmd 1 fp_set_cmd_dt {} 1 {} com_send_cmd {} tm_send_file_part {} {} {} {} 10".format(t0, dt, node, filename, file_id, sf, end_frame[i]))
        cmds.append("rpi_delay 10")
        t0 += dt
    print("Done.")
    total_cmds = len(cmds)
    if (len(cmds)*0.5 > max_cmds):
        total_cmds = max_cmds*2
    with open("file_cmds.txt", 'w') as data_file:
        for i in range(total_cmds):
                data_file.write(cmds[i] + "\n")
