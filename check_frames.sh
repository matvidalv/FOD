#!/usr/bin/env bash
diff <(cd recv_files/ && ls ${1}_*.part | sort -V) <(for (( i=0; i<=$2; i++ )); do echo "$1_${i}_$2.part"; done) | grep .part
