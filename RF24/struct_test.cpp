/*
 * See documentation at https://nRF24.github.io/RF24
 * See License information at root directory of this library
 * Author: Brendan Doherty (2bndy5)
 */

/**
 * A simple example of sending data from 1 nRF24L01 transceiver to another.
 *
 * This example was written to be used on 2 devices acting as "nodes".
 * Use `ctrl+c` to quit at any time.
 */
#include <ctime>       // time()
#include <iostream>    // cin, cout, endl
#include <string>      // string, getline()
#include <time.h>      // CLOCK_MONOTONIC_RAW, timespec, clock_gettime()
#include <RF24/RF24.h> // RF24, RF24_PA_LOW, delay()
//Added by me:
#include <bitset>

using namespace std;

/****************** Linux ***********************/
// Radio CE Pin, CSN Pin, SPI Speed
// CE Pin uses GPIO number with BCM and SPIDEV drivers, other platforms use their own pin numbering
// CS Pin addresses the SPI bus number at /dev/spidev<a>.<b>
// ie: RF24 radio(<ce_pin>, <a>*10+<b>); spidev1.0 is 10, spidev1.1 is 11 etc..

// Generic:
//RF24 radio(22, 0);
RF24 radio(22, 0, 4000000);
/****************** Linux (BBB,x86,etc) ***********************/
// See http://nRF24.github.io/RF24/pages.html for more information on usage
// See http://iotdk.intel.com/docs/master/mraa/ for more information on MRAA
// See https://www.kernel.org/doc/Documentation/spi/spidev for more information on SPIDEV

// For this example, we'll be using a payload containing
// a single float number that will be incremented
// on every successful transmission
bool report1, report2 = false;
uint32_t start_transmission = 0;
typedef struct {
    uint32_t node=0;
    uint32_t index=0;
    uint32_t date;
    uint32_t time;
    int32_t latitude;
    int32_t longitude;
    int32_t altitude;
    uint32_t num_sats;
} gnss_data_t;
gnss_data_t gnss_data;

typedef struct {
    uint32_t node=0;
    uint32_t index=0;
    int32_t fe_mag_x;
    int32_t fe_mag_y;;
    int32_t fe_mag_z;
    float temp;
} mag_data_t;
mag_data_t mag_data;

void setRole(); // prototype to set the node's role
void master();  // prototype of the TX node's behavior
void slave(uint32_t cmd);   // prototype of the RX node's behavior

// custom defined timer for evaluating transmission time in microseconds
struct timespec startTimer, endTimer;
uint32_t getMicros(); // prototype to get ellapsed time in microseconds

int main(int argc, char** argv) {
    uint32_t femto_address = 0;
    if (argc < 2 || femto_address > 3)
        femto_address = 0;
    else
  	sscanf(argv[1], "%u", &femto_address);
    // perform hardware check
    if (!radio.begin()) {
        cout << "radio hardware is not responding!!" << endl;
        return 0; // quit now
    }

    // to use different addresses on a pair of radios, we need a variable to
    // uniquely identify which address this radio will use to transmit
    //bool radioNumber = 1; // 0 uses address[0] to transmit, 1 uses address[1] to transmit
    uint8_t radioNumber = 3;

    uint8_t address[4][8] = {"Femto 1", "Femto 2", "SUCHAI2", "SUCHAI3"};
    // save on transmission time by setting the radio to only transmit the
    // number of bytes we need to transmit a float
    radio.setPayloadSize(sizeof(gnss_data));//payload)); // float datatype occupies 4 bytes

    // Set the PA Level low to try preventing power supply related problems
    // because these examples are likely run with nodes in close proximity to
    // each other.
    radio.setPALevel(RF24_PA_MAX);//LOW); // RF24_PA_MAX is default.

    // set the TX address of the RX node into the TX pipe
    radio.openWritingPipe(address[radioNumber]);     // always uses pipe 0

    // set the RX address of the TX node into a RX pipe
    radio.openReadingPipe(1, address[femto_address]); // using pipe 1

    radio.setDataRate(RF24_250KBPS); // Added later. Only for plus models.
    radio.setChannel(55); // 2400 + 55 = 2400 MHz

    // For debugging info
    // radio.printDetails();       // (smaller) function that prints raw register values
    // radio.printPrettyDetails(); // (larger) function that prints human readable data

    // ready to execute program now
    //setRole(); // calls master() or slave() based on user input
    master();
    return 0;
}


/**
 * set this node's role from stdin stream.
 * this only considers the first char as input.
 */
void setRole() {
    string input = "";
    while (!input.length()) {
        cout << "*** PRESS 'T' to begin transmitting to the other node\n";
        cout << "*** PRESS 'R' to begin receiving from the other node\n";
        cout << "*** PRESS 'Q' to exit" << endl;
        getline(cin, input);
        if (input.length() >= 1) {
            if (input[0] == 'T' || input[0] == 't')
                master();
            else if (input[0] == 'R' || input[0] == 'r')
                slave(start_transmission);
            else if (input[0] == 'Q' || input[0] == 'q')
                break;
            else
                cout << input[0] << " is an invalid input. Please try again." << endl;
        }
        input = ""; // stay in the while loop
    } // while
} // setRole()


/**
 * make this node act as the transmitter
 */
void master() {
    radio.setPayloadSize(sizeof(uint32_t));
    radio.stopListening();                                          // put radio in TX mode
    report1 = false, report2 = false;
    unsigned int failure = 0;                                       // keep track of failures
    start_transmission = 0;
    while (failure < 2 && !report1) {
        radio.write(&start_transmission, sizeof(uint32_t));
        //report1 = radio.write(&start_transmission, sizeof(uint32_t));         // transmit & save the report
	/*
        if (report1) {
            slave(start_transmission);
        }
	else {
            // payload was not delivered
            //cout << "Transmission failed or timed out" << endl;
            failure++;
        }
	*/
	slave(start_transmission);
        delay(100);
	failure++;
	//cout << "Tx1" << endl;
    }
    //if (!report1)
    //    slave(start_transmission);
    radio.setPayloadSize(sizeof(uint32_t));
    radio.stopListening();
    failure = 0;
    start_transmission = 1;
    while (failure < 2 && !report2) {
        radio.write(&start_transmission, sizeof(uint32_t));
        //report2 = radio.write(&start_transmission, sizeof(uint32_t));         // transmit & save the report
	/*
        if (report2) {
            slave(start_transmission);
        }
	else {
            // payload was not delivered
            //cout << "Transmission failed or timed out" << endl;
            failure++;
        }
	*/
        slave(start_transmission);
        delay(100);
	failure++;
	//cout << "Tx2" << endl;
    }
    //if (!report2)
    //    slave(start_transmission);
    //cout << report1 << " " << report2 << endl;
    if (report1 && report2) {
        cout << gnss_data.node << " " << gnss_data.index << " ";
        cout << gnss_data.date << " " << gnss_data.time << " ";
        cout << gnss_data.latitude << " " << gnss_data.longitude << " ";
        cout << gnss_data.altitude << " " << gnss_data.num_sats << " ";
        cout << mag_data.node << " " << mag_data.index << " ";
        cout << mag_data.fe_mag_x << " " << mag_data.fe_mag_y << " ";
        cout << mag_data.fe_mag_z << " " << mag_data.temp << endl;
    }
    else
	exit(1);
    //cout << failure << " failures detected. Leaving TX role." << endl;
    exit(0);
}

/**
 * make this node act as the receiver
 */
void slave(uint32_t cmd) {
    if (cmd == 0) {
        radio.setPayloadSize(sizeof(gnss_data));
        radio.startListening();                                  // put radio in RX mode
        time_t startTimer = time(nullptr);                       // start a timer
        while (time(nullptr) - startTimer < 1 && !report1) {
            uint8_t pipe;
            if (radio.available(&pipe)) {                        // is there a payload? get the pipe number that recieved it
                uint8_t bytes = radio.getPayloadSize();          // get the size of the payload
                radio.read(&gnss_data, bytes);                     // fetch payload from FIFO
                startTimer = time(nullptr);                      // reset timer
                report1 = true;
            }
        }
    }
    else {
        radio.setPayloadSize(sizeof(mag_data));
        radio.startListening();                                  // put radio in RX mode
        time_t startTimer = time(nullptr);                       // start a timer
        while (time(nullptr) - startTimer < 1 && !report2) {                 // use 6 second timeout
            uint8_t pipe;
            if (radio.available(&pipe)) {                        // is there a payload? get the pipe number that recieved it
                uint8_t bytes = radio.getPayloadSize();          // get the size of the payload
                radio.read(&mag_data, bytes);                     // fetch payload from FIFO
                startTimer = time(nullptr);                      // reset timer
                report2 = true;
            }
        }
    }
    //cout << "Nothing received in 6 seconds. Leaving RX role." << endl;
    //radio.stopListening();
}


/**
 * Calculate the ellapsed time in microseconds
 */
uint32_t getMicros() {
    // this function assumes that the timer was started using
    // `clock_gettime(CLOCK_MONOTONIC_RAW, &startTimer);`

    clock_gettime(CLOCK_MONOTONIC_RAW, &endTimer);
    uint32_t seconds = endTimer.tv_sec - startTimer.tv_sec;
    uint32_t useconds = (endTimer.tv_nsec - startTimer.tv_nsec) / 1000;

    return ((seconds) * 1000 + useconds) + 0.5;
}
