/*
 * See documentation at https://nRF24.github.io/RF24
 * See License information at root directory of this library
 * Author: Brendan Doherty (2bndy5)
 */

/**
 * A simple example of sending data from 1 nRF24L01 transceiver to another.
 *
 * This example was written to be used on 2 devices acting as "nodes".
 * Use `ctrl+c` to quit at any time.
 */
#include <ctime>       // time()
#include <iostream>    // cin, cout, endl
#include <string>      // string, getline()
#include <time.h>      // CLOCK_MONOTONIC_RAW, timespec, clock_gettime()
#include <RF24/RF24.h> // RF24, RF24_PA_LOW, delay()
//Added by me:
#include <bitset>

using namespace std;

/****************** Linux ***********************/
// Radio CE Pin, CSN Pin, SPI Speed
// CE Pin uses GPIO number with BCM and SPIDEV drivers, other platforms use their own pin numbering
// CS Pin addresses the SPI bus number at /dev/spidev<a>.<b>
// ie: RF24 radio(<ce_pin>, <a>*10+<b>); spidev1.0 is 10, spidev1.1 is 11 etc..

// Generic:
//RF24 radio(22, 0);
RF24 radio(22, 0, 4000000);
/****************** Linux (BBB,x86,etc) ***********************/
// See http://nRF24.github.io/RF24/pages.html for more information on usage
// See http://iotdk.intel.com/docs/master/mraa/ for more information on MRAA
// See https://www.kernel.org/doc/Documentation/spi/spidev for more information on SPIDEV

// For this example, we'll be using a payload containing
// a single float number that will be incremented
// on every successful transmission
bool report1, report2 = false;
uint32_t start_transmission = 0;

void master();  // prototype of the TX node's behavior
void slave(uint32_t cmd);   // prototype of the RX node's behavior

// custom defined timer for evaluating transmission time in microseconds
struct timespec startTimer, endTimer;
uint32_t getMicros(); // prototype to get ellapsed time in microseconds

int main(int argc, char** argv) {
    uint32_t femto_address = 0;
    if (argc < 2)
        femto_address = 0;
    else
	sscanf(argv[1], "%u", &femto_address);
    if (femto_address > 3)
	femto_address = 0;
    // perform hardware check
    if (!radio.begin()) {
        cout << "radio hardware is not responding!!" << endl;
        return 0; // quit now
    }
    uint8_t radioNumber = 3;
    uint8_t address[4][8] = {"Femto 1", "Femto 2", "SUCHAI2", "SUCHAI3"};
    radio.setPayloadSize(sizeof(uint32_t));
    radio.setPALevel(RF24_PA_MAX);
    radio.openWritingPipe(address[radioNumber]);
    radio.openReadingPipe(1, address[femto_address]);
    radio.setDataRate(RF24_250KBPS);
    radio.setChannel(55); // 2400 + 55 = 2400 MHz
    master();
    return 0;
}

/**
 * Request the femto-satellite to send a carrier wave.
 */
void master() {
    radio.setPayloadSize(sizeof(uint32_t));
    radio.stopListening();
    uint32_t failure = 0;
    start_transmission = 2;
    while (failure < 3) {
        radio.write(&start_transmission, sizeof(uint32_t));
        delay(100);
	failure++;
    }
    radio.stopListening();
    exit(0);
}
