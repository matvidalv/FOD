#include "app/system/cmdRPI.h"

static const char* tag = "cmdRPI";
uint32_t rpi_first_cmd_idx = 0;
uint32_t rpi_last_cmd_idx = 0;

void cmd_rpi_init(void) {
    rpi_first_cmd_idx = cmd_add("rpi_ask_for_help", rpi_ask_for_help, "%u", 1) - 1;
    cmd_add("rpi_check_camera", rpi_check_camera, "", 0);
    cmd_add("rpi_check_frames", rpi_check_frames, "%u %u", 2);
    cmd_add("rpi_check_i2c", rpi_check_i2c, "", 0);
    cmd_add("rpi_check_leds", rpi_check_leds, "", 0);
    cmd_add("rpi_check_serial", rpi_check_serial, "", 0);
    cmd_add("rpi_check_spi", rpi_check_spi, "", 0);
    cmd_add("rpi_check_status", rpi_check_status, "", 0);
    cmd_add("rpi_delay", rpi_delay, "%u", 1);
    cmd_add("rpi_disable_camera", rpi_disable_camera, "", 0);
    cmd_add("rpi_disable_bluetooth", rpi_disable_bluetooth, "", 0);
    cmd_add("rpi_disable_hdmi", rpi_disable_hdmi, "", 0);
    cmd_add("rpi_disable_i2c", rpi_disable_i2c, "", 0);
    cmd_add("rpi_disable_led", rpi_disable_led, "", 0);
    cmd_add("rpi_disable_spi", rpi_disable_spi, "", 0);
    cmd_add("rpi_disable_wifi", rpi_disable_wifi, "", 0);
    cmd_add("rpi_enable_camera", rpi_enable_camera, "", 0);
    cmd_add("rpi_enable_bluetooth", rpi_enable_bluetooth, "", 0);
    cmd_add("rpi_enable_hdmi", rpi_enable_hdmi, "", 0);
    cmd_add("rpi_enable_i2c", rpi_enable_i2c, "", 0);
    cmd_add("rpi_enable_led", rpi_enable_led, "", 0);
    cmd_add("rpi_enable_spi", rpi_enable_spi, "", 0);
    cmd_add("rpi_enable_wifi", rpi_enable_wifi, "", 0);
    cmd_add("rpi_fp", rpi_fp, "%s", 1);
    cmd_add("rpi_node_detect", rpi_node_detect, "%u %u", 2);
    cmd_add("rpi_record", rpi_record, "", 0);
    cmd_add("rpi_system", rpi_system, "%u %s", 2);
    cmd_add("rpi_take_picture", rpi_take_picture, "", 0);
    cmd_add("rpi_tequila", rpi_tequila, "%u %s", 2);
    rpi_last_cmd_idx = cmd_add("rpi_help", rpi_help, "", 0);
}

int rpi_ask_for_help(char *fmt, char *params, int nparams) {
    char curr_cmd_name[2048];
    char buff[50];
    uint32_t node;
    int i;
    LOGI(tag, "Sending command names from 0 to %u:", rpi_last_cmd_idx);
    if (params == NULL) {
	LOGE(tag, "NULL params!");
	return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &node) == nparams) {
        com_send_debug(node, "Help is on the way!", 21);
	osDelay(500);
        for(i=0; i<rpi_last_cmd_idx; i++) {
	    sprintf(buff, "%s\r\n", cmd_get_name(i));
	    strcat(curr_cmd_name, buff);
	    osDelay(10);
        }
        com_send_debug(node, curr_cmd_name, strlen(curr_cmd_name));
    }
    else {
	LOGE(tag, "Invalid params!");
	return CMD_SYNTAX_ERROR;
    }
    memset(&curr_cmd_name[0], 0, sizeof(curr_cmd_name));
    return CMD_OK;
}

int rpi_check_camera(char *fmt, char *params, int nparams) {
    LOGI(tag, "Checking the camera's status...");
    FILE *rpi_f;
    char rpi_output[4];
    uint32_t status;
    #ifdef RPI
    rpi_f = popen("sudo raspi-config nonint get_camera", "r");
    fgets(rpi_output, sizeof(rpi_output), rpi_f);
    status = (uint32_t)rpi_output[0] - 48;
    LOGI(tag, "Status (0 is enabled): %u", status);
    dat_set_system_var(dat_rpi_camera, status);
    pclose(rpi_f);
    #endif
    LOGI(tag, "Done");
    com_send_debug(10, rpi_output, strlen(rpi_output));
    return CMD_OK;
}

int rpi_check_frames(char *fmt, char *params, int nparams) {
    FILE *rpi_f;
    uint32_t file_id, frame, max;
    char rpi_output[2048] = {0};
    char buff[64] = {0};
    char linux_cmd[128] = {0};
    uint32_t node;
    uint32_t index;
    int remaining_space = (int) (sizeof(rpi_output) - sizeof(buff));
    if (params == NULL) {
	LOGE(tag, "NULL params!");
	return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &file_id, &node) == nparams) {
	LOGI(tag, "Searching for file id %u", file_id);
	snprintf(linux_cmd, sizeof(linux_cmd), "ls recv_files/ | grep %u_", file_id);
	LOGI(tag, "Executing: %s", linux_cmd);
        rpi_f = popen(linux_cmd, "r");
        fgets(buff, sizeof(buff), rpi_f);
	sscanf(buff, "%u_%u_%u.part", &file_id, &frame, &max);
        snprintf(linux_cmd, sizeof(linux_cmd), "bash check_frames.sh %u %u", file_id, max);
	LOGI(tag, "Executing: %s", linux_cmd);
	rpi_f = popen(linux_cmd, "r");
	LOGI(tag, "Output:");
	while (fgets(buff, sizeof(buff), rpi_f) != NULL) {
	    strcat(rpi_output, buff);
	    strcat(rpi_output, "\r");
	    if ((int) strlen(rpi_output) > remaining_space) {
		printf("%s", rpi_output);
		LOGI(tag, "Sending output to node %u", node);
		com_send_debug(node, rpi_output, strlen(rpi_output));
		memset(&buff[0], 0, sizeof(buff));
		memset(&rpi_output[0], 0, sizeof(rpi_output));
	    }
        }
	if ((int) strlen(rpi_output) > 0) {
	    printf("%s", rpi_output);
	    LOGI(tag, "Sending output to node %u", node);
	    com_send_debug(node, rpi_output, strlen(rpi_output));
	}
	pclose(rpi_f);
	memset(&buff[0], 0, sizeof(buff));
        memset(&linux_cmd[0], 0, sizeof(linux_cmd));
        memset(&rpi_output[0], 0, sizeof(rpi_output));
    }
    else {
	LOGE(tag, "Invalid params!");
	return CMD_SYNTAX_ERROR;
    }
    return CMD_OK;
}

int rpi_check_i2c(char *fmt, char *params, int nparams) {
    LOGI(tag, "Checking the I2C status...");
    FILE *rpi_f;
    char rpi_output[4];
    uint32_t status;
    #ifdef RPI
    rpi_f = popen("sudo raspi-config nonint get_i2c", "r");
    fgets(rpi_output, sizeof(rpi_output), rpi_f);
    status = (uint32_t)rpi_output[0] - 48;
    LOGI(tag, "Status (0 is enabled): %u", status);
    dat_set_system_var(dat_rpi_i2c, status);
    pclose(rpi_f);
    #endif
    LOGI(tag, "Done");
    com_send_debug(10, rpi_output, strlen(rpi_output));
    return CMD_OK;
}

int rpi_check_leds(char *fmt, char *params, int nparams) {
    LOGI(tag, "Checking the LEDs status...");
    FILE *rpi_f;
    char rpi_output[4];
    uint32_t status;
    #ifdef RPI
    rpi_f = popen("sudo raspi-config nonint get_leds", "r");
    fgets(rpi_output, sizeof(rpi_output), rpi_f);
    status = (uint32_t)rpi_output[0] - 48;
    LOGI(tag, "Status (0 is enabled): %u", status);
    dat_set_system_var(dat_rpi_leds, status);
    pclose(rpi_f);
    #endif
    LOGI(tag, "Done");
    com_send_debug(10, rpi_output, strlen(rpi_output));
    return CMD_OK;
}

int rpi_check_serial(char *fmt, char *params, int nparams) {
    LOGI(tag, "Checking the Serial status...");
    FILE *rpi_f;
    char rpi_output[4];
    uint32_t status;
    #ifdef RPI
    rpi_f = popen("sudo raspi-config nonint get_serial", "r");
    fgets(rpi_output, sizeof(rpi_output), rpi_f);
    status = (uint32_t)rpi_output[0] - 48;
    LOGI(tag, "Status (0 is enabled): %u", status);
    dat_set_system_var(dat_rpi_serial, status);
    pclose(rpi_f);
    #endif
    LOGI(tag, "Done");
    com_send_debug(10, rpi_output, strlen(rpi_output));
    return CMD_OK;
}

int rpi_check_spi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Checking the SPI status...");
    FILE *rpi_f;
    char rpi_output[4];
    uint32_t status;
    #ifdef RPI
    rpi_f = popen("sudo raspi-config nonint get_spi", "r");
    fgets(rpi_output, sizeof(rpi_output), rpi_f);
    status = (uint32_t)rpi_output[0] - 48;
    LOGI(tag, "Status (0 is enabled): %u", status);
    dat_set_system_var(dat_rpi_spi, status);
    pclose(rpi_f);
    #endif
    LOGI(tag, "Done");
    com_send_debug(10, rpi_output, strlen(rpi_output));
    return CMD_OK;
}

int rpi_check_status(char *fmt, char *params, int nparams) {
    char buff[64];
    LOGI(tag, "Checking the RPI's status...");
    uint32_t camera_status = dat_get_system_var(dat_rpi_camera);
    uint32_t i2c_status = dat_get_system_var(dat_rpi_i2c);
    uint32_t leds_status = dat_get_system_var(dat_rpi_leds);
    uint32_t serial_status = dat_get_system_var(dat_rpi_serial);
    uint32_t spi_status = dat_get_system_var(dat_rpi_spi);
    LOGI(tag, "Raspberry pi status:");
    sprintf(buff, "Camera: %u\r\nI2C: %u\r\nLEDs: %u\r\nSerial: %u\r\nSPI: %u\r\n", camera_status,
		                                                                    i2c_status,
			    					                    leds_status,
						                                    serial_status,
						                                    spi_status);
    printf("%s", buff);
    com_send_debug(10, buff, strlen(buff));
    return CMD_OK;
}

int rpi_delay(char *fmt, char *params, int nparams) {
    uint32_t delta;
    if (params == NULL) {
	LOGE(tag, "NULL params!");
	return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &delta) == nparams) {
	osDelay(delta);
    }
    else {
	LOGE(tag, "Invalid params!");
	return CMD_SYNTAX_ERROR;
    }
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_disable_camera(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling camera");
    #ifdef RPI
    system("sudo raspi-config nonint do_camera 1");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_disable_bluetooth(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling bluetooth");
    #ifdef RPI
    system("sudo rfkill block bluetooth");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_disable_hdmi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling HDMI");
    #ifdef RPI
    system("/usr/bin/tvservice -o");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_disable_i2c(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling I2C");
    #ifdef RPI
    system("sudo raspi-config nonint do_i2c 1");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_disable_led(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling LED");
    #ifdef RPI
    system("sudo raspi-config nonint do_leds 1");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_disable_spi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling SPI");
    #ifdef RPI
    system("sudo raspi-config nonint do_spi 1");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_disable_wifi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling wifi");
    #ifdef RPI
    system("sudo rfkill block wifi");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_enable_camera(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling camera");
    #ifdef RPI
    system("sudo raspi-config nonint do_camera 0");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_enable_bluetooth(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling bluetooth");
    #ifdef RPI
    system("sudo rfkill unblock bluetooth");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_enable_hdmi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling HDMI");
    #ifdef RPI
    system("/usr/bin/tvservice -p");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_enable_i2c(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling I2C");
    #ifdef RPI
    system("sudo raspi-config nonint do_i2c 0");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_enable_led(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling LED");
    #ifdef RPI
    system("sudo raspi-config nonint do_leds 0");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_enable_spi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling SPI");
    #ifdef RPI
    system("sudo raspi-config nonint do_spi 0");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_enable_wifi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling wifi");
    #ifdef RPI
    system("sudo rfkill unblock wifi");
    LOGI(tag, "Done")
    #endif
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_fp(char *fmt, char *params, int nparams) {
    FILE *rpi_f;
    uint32_t node;
    uint32_t index;
    char buff[128] = {0};
    char path2fp[128] = {0};
    char send2node[256] = {0};
    if (params == NULL) {
	LOGE(tag, "NULL params!");
	return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &path2fp) == nparams) {
	LOGI(tag, "Executing commands from: %s", &path2fp);
	rpi_f = fopen(path2fp, "r");
	while (fgets(buff, sizeof(buff), rpi_f) != NULL) {
	    if (strchr(buff, ':') == NULL) {
	        cmd_t *new_cmd = cmd_build_from_str(buff);
	        cmd_send(new_cmd);
	        LOGI(tag, "%s", buff);
	    }
	    else {
		sscanf(buff, "%u:%s", &node, send2node);
	        memset(&send2node[0], 0, sizeof(send2node));
		index = log10(node) + 2;
		snprintf(send2node, sizeof(send2node), "com_send_cmd %u %s", node, &buff[index]);
	        cmd_t *new_cmd = cmd_build_from_str(send2node);
	        cmd_send(new_cmd);
	        LOGI(tag, "%s", send2node);
	        memset(&send2node[0], 0, sizeof(send2node));
	    }
	    //osDelay(1000);
	    memset(&buff[0], 0, sizeof(buff));
        }
	fclose(rpi_f);
    }
    else {
	LOGE(tag, "Invalid params!");
	return CMD_SYNTAX_ERROR;
    }
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_node_detect(char *fmt, char *params, int nparams) {
    uint32_t last_node;
    uint32_t send_to;
    char curr_node[22];
    if (params == NULL) {
	LOGE(tag, "NULL params!");
	return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &send_to, &last_node) == nparams) {
	int rc = 0;
	for (int i=0; i<last_node; i++) {
            rc = csp_ping((uint8_t) i, 3000, 10, CSP_O_NONE);
	    if (rc > 0) {
                sprintf(curr_node, "Ping to %d took %u", i, rc);
                com_send_debug(send_to, curr_node, strlen(curr_node));
                memset(&curr_node[0], 0, sizeof(curr_node));
	    }
	}
    }
    else {
	LOGE(tag, "Invalid params!");
	return CMD_SYNTAX_ERROR;
    }
    return CMD_OK;
}

int rpi_record(char *fmt, char *params, int nparams) {
    LOGI(tag, "Recording a video...");
    #ifdef RPI
    char buffer[65];
    char settings[42] = "libcamera-vid -t 10000 -w 1920 -h 1080 -o";
    uint32_t dat_time = dat_get_time();
    time_t t = time(NULL);
    struct tm local = *localtime(&t);
    snprintf(buffer, sizeof(buffer), "%s %d_%d_%d_%u.h264", settings,
		                                            local.tm_year + 1900,
		                                            local.tm_mon + 1,
					                    local.tm_mday,
							    dat_time);
    LOGI(tag, "Executing command %s", buffer);
    system(buffer);
    #endif
    LOGI(tag, "Done");
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_system(char *fmt, char *params, int nparams) {
    FILE *rpi_f;
    char rpi_sys_call[2048] = {0};
    char rpi_output[2048] = {0};
    char buff[256] = {0};
    uint32_t node;
    uint32_t index;
    int remaining_space = (int) (sizeof(rpi_output) - sizeof(buff));
    if (params == NULL) {
	LOGE(tag, "NULL params!");
	return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &node, &rpi_sys_call) == nparams) {
	index = log10(node) + 2;
	LOGI(tag, "Executing: %s", &params[index]);
	rpi_f = popen(&params[index], "r");
	LOGI(tag, "Output:");
	while (fgets(buff, sizeof(buff), rpi_f) != NULL) {
	    strcat(rpi_output, buff);
            strcat(rpi_output, "\r");
	    if ((int) strlen(rpi_output) > remaining_space) {
		printf("%s", rpi_output);
		LOGI(tag, "Sending output to node %u", node);
		com_send_debug(node, rpi_output, strlen(rpi_output));
		memset(&buff[0], 0, sizeof(buff));
		memset(&rpi_output[0], 0, sizeof(rpi_output));
	    }
        }
	if ((int) strlen(rpi_output) > 0) {
	    printf("%s", rpi_output);
	    LOGI(tag, "Sending output to node %u", node);
	    com_send_debug(node, rpi_output, strlen(rpi_output));
	}
	pclose(rpi_f);
	memset(&buff[0], 0, sizeof(buff));
        memset(&rpi_output[0], 0, sizeof(rpi_output));
    }
    else {
	LOGE(tag, "Invalid params!");
	return CMD_SYNTAX_ERROR;
    }
    return CMD_OK;
}

int rpi_take_picture(char *fmt, char *params, int nparams) {
    LOGI(tag, "Taking picture...");
    #ifdef RPI
    char buffer[45];
    uint32_t dat_time = dat_get_time();
    time_t t = time(NULL);
    struct tm local = *localtime(&t);
    snprintf(buffer, sizeof(buffer), "libcamera-jpeg -o %d_%d_%d_%u.jpg", local.tm_year + 1900,
		                                                          local.tm_mon + 1,
							   	          local.tm_mday,
								          dat_time);
    LOGI(tag, "Executing command %s", buffer);
    system(buffer);
    #endif
    LOGI(tag, "Done");
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int rpi_tequila(char *fmt, char *params, int nparams) {
    FILE *rpi_f;
    char rpi_sys_call[2048];
    char rpi_output[1334];
    char buff[64];
    uint32_t node;
    uint32_t index;
    int remaining_space = (int) (1300 - sizeof(buff));
    if (params == NULL) {
	LOGE(tag, "NULL params!");
	return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &node, &rpi_sys_call) == nparams) {
        com_send_debug(node, "Si quieres un banda S...\r", 26);
        osDelay(1000);
	index = log10(node) + 2;
	LOGI(tag, "Executing: %s", &params[index]);
	rpi_f = popen(&params[index], "r");
	strcat(rpi_output, "com_send_cmd 1 com_send_data 17 13 ");
	LOGI(tag, "Output:");
	while (fgets(buff, sizeof(buff), rpi_f) != NULL) {
	    strcat(rpi_output, buff);
            strcat(rpi_output, "\r");
	    if ((int) strlen(rpi_output) > remaining_space) {
		printf("%s", rpi_output);
		LOGI(tag, "Sending output to node %u", node);
                cmd_t *eps_cmd_1 = cmd_build_from_str("com_send_cmd 1 eps_set_output 5 1");
                cmd_send(eps_cmd_1);
		osDelay(50);
                cmd_t *actv_cmd = cmd_build_from_str("com_send_cmd 1 com_send_data 17 7 ACTV");
                cmd_send(actv_cmd);
                cmd_t *tequila_cmd = cmd_build_from_str(rpi_output);
                cmd_send(tequila_cmd);
                cmd_t *idle_cmd = cmd_build_from_str("com_send_cmd 1 com_send_data 17 7 IDLE");
                cmd_send(idle_cmd);
                cmd_t *eps_cmd_0 = cmd_build_from_str("com_send_cmd 1 eps_set_output 5 0");
                cmd_send(eps_cmd_0);
		com_send_debug(node, rpi_output, strlen(rpi_output));
		memset(&buff[0], 0, sizeof(buff));
		memset(&rpi_output[0], 0, sizeof(rpi_output));
	        strcat(rpi_output, "com_send_cmd 1 com_send_data 17 13 ");
	    }
        }
	if ((int) strlen(rpi_output) > 0) {
	    printf("%s", rpi_output);
	    LOGI(tag, "Sending output to node %u", node);
            cmd_t *eps_cmd_1 = cmd_build_from_str("com_send_cmd 1 eps_set_output 5 1");
            cmd_send(eps_cmd_1);
	    osDelay(50);
            cmd_t *actv_cmd = cmd_build_from_str("com_send_cmd 1 com_send_data 17 7 ACTV");
            cmd_send(actv_cmd);
            cmd_t *tequila_cmd = cmd_build_from_str(rpi_output);
            cmd_send(tequila_cmd);
            cmd_t *idle_cmd = cmd_build_from_str("com_send_cmd 1 com_send_data 17 7 IDLE");
            cmd_send(idle_cmd);
            cmd_t *eps_cmd_0 = cmd_build_from_str("com_send_cmd 1 eps_set_output 5 0");
            cmd_send(eps_cmd_0);
	    memset(&buff[0], 0, sizeof(buff));
	    memset(&rpi_output[0], 0, sizeof(rpi_output));
	}
	pclose(rpi_f);
	memset(&buff[0], 0, sizeof(buff));
        memset(&rpi_output[0], 0, sizeof(rpi_output));
        com_send_debug(node, "...se necesita un cable y ya!.\n", 32);
    }
    else {
	LOGE(tag, "Invalid params!");
	return CMD_SYNTAX_ERROR;
    }
    return CMD_OK;
}

int rpi_help(char *fmt, char *params, int nparams) {
    const char *rpi_cmd_name;
    const char *rpi_cmd_fmt;
    int i;
    LOGI(tag, "Printing commands from %u to %u:", rpi_first_cmd_idx, rpi_last_cmd_idx-1);
    printf("%5s %s %25s\r\n", "Index", "Name", "Params");
    for(i=rpi_first_cmd_idx; i<rpi_last_cmd_idx; i++) {
	rpi_cmd_name = cmd_get_name(i);
        int printed = printf("%5d %s", i, rpi_cmd_name);
	rpi_cmd_fmt = cmd_get_fmt((char *) rpi_cmd_name);
        if (*rpi_cmd_fmt != '\0')
            printf(" %s\r\n", rpi_cmd_fmt);
        else
            printf("\r\n");
    }
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}
