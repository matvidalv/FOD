#include "app/system/cmdAOA.h"

static const char* tag = "cmdAOA";
uint32_t aoa_first_cmd_idx = 0;
uint32_t aoa_last_cmd_idx = 0;
uint32_t aoa_power_on;
float aoa_ver = 4.220228;

void cmd_aoa_init(void) {
    aoa_first_cmd_idx = cmd_add("aoa_get_data", aoa_get_data, "", 0) - 1;
    cmd_add("aoa_get_version", aoa_get_version, "", 0);
    cmd_add("aoa_pwr", aoa_pwr, "%u", 1);
    cmd_add("aoa_reset", aoa_reset, "", 0);
    aoa_last_cmd_idx = cmd_add("aoa_help", aoa_help, "", 0);
}

int aoa_get_data(char *fmt, char *params, int nparams) {
    system("sudo ./../RF24/examples_linux/requestCarrierWave");
    aoa_data_t aoa_data;
    aoa_data.index = dat_get_system_var(dat_drp_idx_aoa);
    aoa_data.timestamp = dat_get_time();
    #ifdef RPI
    LOGI(tag, "Initializing the RPI pins and SPI...");
    bcm2835_init();
    bcm2835_gpio_write(AOA_CS, HIGH);
    bcm2835_aux_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
    // bcm2835_aux_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_65536);
    bcm2835_aux_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_64);
    bcm2835_spi_chipSelect(BCM2835_SPI_CS_NONE);
    //bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);
    LOGI(tag, "Done")
    LOGI(tag, "Reading the AOA data");
    uint8_t read_v_mag1 = 0b00000000;
    uint8_t read_v_phase1 = 0b00001000;
    uint8_t read_v_mag2 = 0b00010000;
    uint8_t read_v_phase2 = 0b00011000;
    uint8_t read_v_mag1_[2]={read_v_mag1, 0x00};
    uint8_t read_v_phase1_[2]={read_v_phase1, 0x00};
    uint8_t read_v_mag2_[2]={read_v_mag2, 0x00};
    uint8_t read_v_phase2_[2]={read_v_phase2, 0x00};
    bcm2835_gpio_write(AOA_CS, LOW);
    bcm2835_aux_spi_transfern(read_v_mag1_, 2);
    bcm2835_aux_spi_transfern(read_v_phase1_, 2);
    bcm2835_aux_spi_transfern(read_v_mag2_, 2);
    bcm2835_aux_spi_transfern(read_v_phase2_, 2);
    bcm2835_gpio_write(AOA_CS, HIGH);
    bcm2835_aux_spi_end();
    /* Transform two bytes into legible data */
    read_v_mag1_[0] = read_v_mag1_[0] << 4;
    read_v_mag1_[1] = read_v_mag1_[1] >> 4;
    uint8_t data_1 = read_v_mag1_[0] + read_v_mag1_[1];
    read_v_phase1_[0] = read_v_phase1_[0] << 4;
    read_v_phase1_[1] = read_v_phase1_[1] >> 4;
    uint8_t data_2 = read_v_phase1_[0] + read_v_phase1_[1];
    read_v_mag2_[0] = read_v_mag2_[0] << 4;
    read_v_mag2_[1] = read_v_mag2_[1] >> 4;
    uint8_t data_3 = read_v_mag2_[0] + read_v_mag2_[1];
    read_v_phase2_[0] = read_v_phase2_[0] << 4;
    read_v_phase2_[1] = read_v_phase2_[1] >> 4;
    uint8_t data_4 = read_v_phase2_[0] + read_v_phase2_[1];
    LOGI(tag, "Voltage magnitude 1: %d", data_1);
    LOGI(tag, "Voltage phase     1: %d", data_2);
    LOGI(tag, "Voltage magnitude 2: %d", data_3);
    LOGI(tag, "Voltage phase     2: %d", data_4);
    aoa_data.v_mag1 = data_1;
    aoa_data.v_phase1 = data_2;
    aoa_data.v_mag2 = data_3;
    aoa_data.v_phase2 = data_4;
    #endif
    int rc = dat_add_payload_sample(&aoa_data, aoa_sensors);
    return rc != -1 ? CMD_OK : CMD_ERROR;
}

int aoa_get_version(char *fmt, char *params, int nparams) {
    LOGI(tag, "Getting AOA's version");
    LOGI(tag, "AOA's version: %f", aoa_ver);
    return CMD_OK;
}

int aoa_pwr(char *fmt, char *params, int nparams) {
    if (params == NULL) {
	LOGE(tag, "NULL params!");
        return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &aoa_power_on) == nparams) {
	#ifdef RPI
        bcm2835_init();
        bcm2835_gpio_fsel(AOA_POWER_PIN, BCM2835_GPIO_FSEL_OUTP);
        bcm2835_gpio_fsel(AOA_IRQ_PIN, BCM2835_GPIO_FSEL_INPT);
        #endif
	if (aoa_power_on) {
            #ifdef RPI
            bcm2835_gpio_write(AOA_POWER_PIN, HIGH);
            #endif
	    LOGI(tag, "AOA is powered on");
	}
	else {
            #ifdef RPI
            bcm2835_gpio_write(AOA_POWER_PIN, LOW);
            #endif
	    LOGI(tag, "AOA is powered off");
	}
	dat_set_system_var(dat_aoa_power_on, aoa_power_on);
    }
    else {
	LOGE(tag, "Invalid params!");
        return CMD_SYNTAX_ERROR;
    }
    return CMD_OK;
}

int aoa_reset(char *fmt, char *params, int nparams) {
    LOGI(tag, "Reseting AOA to default values");
    dat_set_system_var(dat_aoa_power_on, 0);
    dat_set_system_var(dat_drp_idx_aoa, 0);
    dat_set_system_var(dat_drp_ack_aoa, 0);
    return CMD_OK;
}

int aoa_help(char *fmt, char *params, int nparams) {
    const char *aoa_cmd_name;
    const char *aoa_cmd_fmt;
    int i;
    LOGI(tag, "Printing commands from %u to %u:", aoa_first_cmd_idx, aoa_last_cmd_idx-1);
    printf("%5s %s %25s\r\n", "Index", "Name", "Params");
    for(i=aoa_first_cmd_idx; i<aoa_last_cmd_idx; i++) {
	aoa_cmd_name = cmd_get_name(i);
        int printed = printf("%5d %s", i, aoa_cmd_name);
	aoa_cmd_fmt = cmd_get_fmt((char *) aoa_cmd_name);
        if (*aoa_cmd_fmt != '\0')
            printf(" %s\r\n", aoa_cmd_fmt);
        else
            printf("\r\n");
    }
    return CMD_OK;
}
