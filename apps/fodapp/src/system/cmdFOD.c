#include "app/system/cmdFOD.h"

static const char* tag = "cmdFOD";
uint32_t first_cmd_idx = 0;
uint32_t last_cmd_idx = 0;
uint32_t on_time;
uint32_t power_on;
uint32_t released = 0;
float ver = 4.220306;

void cmd_fod_init(void) {
    first_cmd_idx = cmd_add("fod_deploy", deploy_femtosats, "", 0) - 1;
    cmd_add("fod_en_data_acquisition", fod_en_data_acquisition, "", 0);
    cmd_add("fod_force_deployment", fod_force_deployment, "%u", 1);
    cmd_add("fod_get_config", fod_get_config, "", 0);
    cmd_add("fod_get_femtosat_data", get_femtosat_data, "", 0);
    cmd_add("fod_get_status", fod_get_status, "", 0);
    cmd_add("fod_get_version", fod_get_version, "", 0);
    cmd_add("fod_pwr", fod_pwr, "%u", 1);
    cmd_add("fod_reset", fod_reset, "", 0);
    cmd_add("fod_send_beacon", fod_send_beacon, "", 0);
    cmd_add("fod_set_on_time", set_on_time, "%u", 1);
    cmd_add("fod_sleep_femto", fod_sleep_femto, "%u %u", 2);
    last_cmd_idx = cmd_add("fod_help", fod_help, "", 0);
}

int deploy_femtosats(char *fmt, char *params, int nparams) {
    cmd_t *fod_pwr_on_cmd = cmd_build_from_str("fod_pwr 1");
    cmd_send(fod_pwr_on_cmd);
    LOGI(tag, "Deploying femto-satellites");
    struct timeval stop, start;
    uint32_t dt = 0;
    gettimeofday(&start, NULL);
    #ifdef RPI
    on_time = dat_get_system_var(dat_fod_on_time);
    released = bcm2835_gpio_lev(FOD_STATUS_PIN);
    while (dt < on_time && !released) {
        bcm2835_gpio_write(FOD_DEPLOY_PIN, HIGH);
        osDelay(10);
        released = bcm2835_gpio_lev(FOD_STATUS_PIN);
        gettimeofday(&stop, NULL);
        dt = (stop.tv_sec - start.tv_sec)*1000 + (stop.tv_usec - start.tv_usec)*0.001;
    }
    bcm2835_gpio_write(FOD_DEPLOY_PIN, LOW);
    #endif
    LOGI(tag, "Duration: %u ms", dt);
    uint32_t attempts = dat_get_system_var(dat_fod_attempts) + 1;
    LOGI(tag, "Attempt N°%u", attempts);
    LOGI(tag, "Released: %u", released);
    char result[51];
    sprintf(result, "Duration: %u ms\r\nAttempt N°%u\r\nReleased: %u\r\n", dt, attempts, released);
    com_send_debug(10, result, strlen(result));
    dat_set_system_var(dat_fod_attempts, attempts);
    dat_set_system_var(dat_fod_released, released);
    cmd_t *fod_pwr_off_cmd = cmd_build_from_str("fod_pwr 0");
    cmd_send(fod_pwr_off_cmd);
    return CMD_OK;
}

int fod_en_data_acquisition(char *fmt, char *params, int nparams) {
    LOGI(tag, "Setting dat_fod_request_data to true");
    dat_set_system_var(dat_fod_request_data, 1);
    return CMD_OK;
}

int fod_force_deployment(char *fmt, char *params, int nparams) {
    cmd_t *fod_pwr_on_cmd = cmd_build_from_str("fod_pwr 1");
    cmd_send(fod_pwr_on_cmd);
    LOGI(tag, "Deploying femto-satellites");
    struct timeval stop, start;
    uint32_t dt = 0;
    uint32_t attempts, duration;
    if (params == NULL) {
	LOGE(tag, "NULL params!");
        return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &duration) == nparams) {
        gettimeofday(&start, NULL);
        #ifdef RPI
        while (dt < duration) {
            bcm2835_gpio_write(FOD_DEPLOY_PIN, HIGH);
            osDelay(10);
            gettimeofday(&stop, NULL);
            dt = (stop.tv_sec - start.tv_sec)*1000 + (stop.tv_usec - start.tv_usec)*0.001;
        }
        bcm2835_gpio_write(FOD_DEPLOY_PIN, LOW);
        released = bcm2835_gpio_lev(FOD_STATUS_PIN);
        #endif
        LOGI(tag, "Duration: %u ms", dt);
        attempts = dat_get_system_var(dat_fod_attempts) + 1;
        LOGI(tag, "Attempt N°%u", attempts);
        LOGI(tag, "Released: %u", released);
        dat_set_system_var(dat_fod_attempts, attempts);
        dat_set_system_var(dat_fod_released, released);
        char result[51];
        sprintf(result, "Duration: %u ms\r\nAttempt N°%u\r\nReleased: %u\r\n", dt, attempts, released);
        com_send_debug(10, result, strlen(result));
    }
    else {
	LOGE(tag, "Invalid params!");
        return CMD_SYNTAX_ERROR;
    }
    cmd_t *fod_pwr_off_cmd = cmd_build_from_str("fod_pwr 0");
    cmd_send(fod_pwr_off_cmd);
    return CMD_OK;
}

int fod_get_config(char *fmt, char *params, int nparams) {
    LOGI(tag, "Getting FOD's configuration");
    uint32_t released = dat_get_system_var(dat_fod_released);
    uint32_t on_time = dat_get_system_var(dat_fod_on_time);
    uint32_t power_on = dat_get_system_var(dat_fod_power_on);
    uint32_t attempts = dat_get_system_var(dat_fod_attempts);
    bool request_data = dat_get_system_var(dat_fod_request_data);
    LOGI(tag, "Reading FOD data:");
    char buff[120];
    snprintf(buff, sizeof(buff),
	     "Version: %f\r\nReleased: %u\r\nOn time: %u ms\r\nPowered on: %u\r\nAttempts: %u\r\nRequest data: %u\r\n",
             ver, released, on_time, power_on, attempts, request_data);
    printf("%s", buff);
    com_send_debug(10, buff, strlen(buff));
    return CMD_OK;
}

int get_femtosat_data(char *fmt, char *params, int nparams) {
    uint32_t femto_address = 0;
    fod_data_t fe_data;
    fe_data.index = dat_get_system_var(dat_drp_idx_fod);
    fe_data.timestamp = dat_get_time();
    uint32_t fe_msg_len = 81;
    LOGI(tag, "Requesting data to the femto-satellite");
    FILE *fp;
    char femto_msg[fe_msg_len];
    if (params == NULL) {
	LOGE(tag, "NULL params!");
        return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &femto_address) == nparams) {
	char femto_cmd[45];
	sprintf(femto_cmd, "sudo ./../RF24/examples_linux/struct_test %u", femto_address);
        fp = popen(femto_cmd, "r");
        if (fp == NULL) {
            return CMD_ERROR;
        }
        while (fgets(femto_msg, fe_msg_len, fp) != NULL) {
            LOGI(tag, "Received: %s", femto_msg);
            //memcpy(&fe_data+8, femto_msg, 32+20);
            sscanf(femto_msg, "%u %u %u %u %d %d %d %u %u %u %d %d %d %f", &fe_data.node1,
            		                                                   &fe_data.fe_index1,
            		                                                   &fe_data.date,
            		                                                   &fe_data.time,
            					                           &fe_data.latitude,
            						                   &fe_data.longitude,
            		                                                   &fe_data.altitude,
            							           &fe_data.num_sats,
            						                   &fe_data.node2,
            							           &fe_data.fe_index2,
            						                   &fe_data.fe_mag_x,
            						                   &fe_data.fe_mag_y,
            						                   &fe_data.fe_mag_z,
            							           &fe_data.temp);
        }
        pclose(fp);
        if (&femto_msg[0] == NULL || femto_msg[0] == 'R') {
            LOGI(tag, "No data received");
            return CMD_ERROR;
        }
        LOGI(tag, "Getting femto-satellites' data");
    }
    else {
	LOGE(tag, "Invalid params!");
        return CMD_SYNTAX_ERROR;
    }
    int rc = dat_add_payload_sample(&fe_data, fod_sensors);
    com_send_debug(10, "Done", strlen("Done"));
    return rc != -1 ? CMD_OK : CMD_ERROR;
}

int fod_get_status(char *fmt, char *params, int nparams) {
    LOGI(tag, "Getting FOD's status");
    #ifdef RPI
    released = bcm2835_gpio_lev(FOD_STATUS_PIN);
    #endif
    dat_set_system_var(dat_fod_released, released);
    char buff[14];
    sprintf(buff, "Released: %u", released);
    LOGI(tag, buff);
    com_send_debug(10, buff, strlen(buff));
    return CMD_OK;
}

int fod_get_version(char *fmt, char *params, int nparams) {
    LOGI(tag, "Getting FOD's version");
    char buff[26];
    sprintf(buff, "FOD's version: %f", ver);
    LOGI(tag, buff);
    com_send_debug(10, buff, strlen(buff));
    return CMD_OK;
}

int fod_pwr(char *fmt, char *params, int nparams) {
    char buff[19];
    if (params == NULL) {
	LOGE(tag, "NULL params!");
        return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &power_on) == nparams) {
	#ifdef RPI
        bcm2835_init();
        bcm2835_gpio_fsel(FOD_DEPLOY_PIN, BCM2835_GPIO_FSEL_OUTP);
        bcm2835_gpio_fsel(FOD_POWER_PIN, BCM2835_GPIO_FSEL_OUTP);
        bcm2835_gpio_fsel(FOD_STATUS_PIN, BCM2835_GPIO_FSEL_INPT);
        bcm2835_gpio_write(FOD_DEPLOY_PIN, LOW);
        #endif
	if (power_on) {
            #ifdef RPI
            bcm2835_gpio_write(FOD_POWER_PIN, HIGH);
            #endif
	    sprintf(buff, "FOD is powered on");
	}
	else {
            #ifdef RPI
            bcm2835_gpio_write(FOD_POWER_PIN, LOW);
            #endif
	    sprintf(buff, "FOD is powered off");
	}
	LOGI(tag, buff);
        com_send_debug(10, buff, strlen(buff));
	dat_set_system_var(dat_fod_power_on, power_on);
    }
    else {
	LOGE(tag, "Invalid params!");
        return CMD_SYNTAX_ERROR;
    }
    return CMD_OK;
}

int fod_reset(char *fmt, char *params, int nparams) {
    LOGI(tag, "Reseting FOD to default values");
    dat_set_system_var(dat_fod_attempts, 0);
    dat_set_system_var(dat_fod_on_time, 3000);
    dat_set_system_var(dat_fod_power_on, 0);
    dat_set_system_var(dat_fod_released, 0);
    dat_set_system_var(dat_fod_request_data, 0);
    dat_set_system_var(dat_drp_idx_fod, 0);
    dat_set_system_var(dat_drp_ack_fod, 0);
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int fod_send_beacon(char *fmt, char *params, int nparams) {
    LOGI(tag, "Sending beacon");
    return CMD_OK;
}

int set_on_time(char *fmt, char *params, int nparams) {
    if (params == NULL) {
	LOGE(tag, "NULL params!");
        return CMD_SYNTAX_ERROR;
    }
    
    if (sscanf(params, fmt, &on_time) == nparams) {
	char buff[25];
	sprintf(buff, "Setting on time to %u", on_time);
	LOGI(tag, buff);
	com_send_debug(10, buff, strlen(buff));
	dat_set_system_var(dat_fod_on_time, on_time);
    }
    else {
	LOGE(tag, "Invalid params!");
        return CMD_SYNTAX_ERROR;
    }
    return CMD_OK;
}

int fod_sleep_femto(char *fmt, char *params, int nparams) {
    uint32_t femto_address = 0;
    uint32_t sleep_time = 300;
    char femto_cmd[45];
    if (sscanf(params, fmt, &femto_address, &sleep_time) == nparams) {
        sprintf(femto_cmd, "sudo ./../RF24/examples_linux/sleepFemto %u %u", femto_address, sleep_time);
        system(femto_cmd);
    }
    else {
        LOGE(tag, "Invalid params!");
        return CMD_SYNTAX_ERROR;
    }
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}

int fod_help(char *fmt, char *params, int nparams) {
    const char *fod_cmd_name;
    const char *fod_cmd_fmt;
    int i;
    LOGI(tag, "Printing commands from %u to %u:", first_cmd_idx, last_cmd_idx-1);
    printf("%5s %s %25s\r\n", "Index", "Name", "Params");
    for(i=first_cmd_idx; i<last_cmd_idx; i++) {
	fod_cmd_name = cmd_get_name(i);
        int printed = printf("%5d %s", i, fod_cmd_name);
	fod_cmd_fmt = cmd_get_fmt((char *) fod_cmd_name);
        if (*fod_cmd_fmt != '\0')
            printf(" %s\r\n", fod_cmd_fmt);
        else
            printf("\r\n");
    }
    com_send_debug(10, "Done", strlen("Done"));
    return CMD_OK;
}
