#include "app/system/taskFOD.h"

void taskFOD(void *params){
    dat_set_system_var(dat_fod_request_data, 0);
    osDelay(50);
    while (1) {
        if (dat_get_system_var(dat_fod_request_data)) {
            cmd_t *fod_pwr_cmd = cmd_build_from_str("fod_pwr 1");
            cmd_send(fod_pwr_cmd);
	    osDelay(100);
            for (int i=0; i<400; i++) {
                cmd_t *fod_get_data_cmd = cmd_build_from_str("fod_get_femtosat_data 0");
                cmd_send(fod_get_data_cmd);
		osDelay(100);
            }
            cmd_t *fod_pwr_off_cmd = cmd_build_from_str("fod_pwr 0");
            cmd_send(fod_pwr_off_cmd);
        }
        dat_set_system_var(dat_fod_request_data, 0);
	osDelay(10000);
    }
    /*
    while(1){
        osDelay(10000);
        LOGI("taskFOD", "Sending beacon!");
        cmd_t *cmd = cmd_build_from_str("fod_send_beacon");
        cmd_send(cmd);
    }
    */
}
