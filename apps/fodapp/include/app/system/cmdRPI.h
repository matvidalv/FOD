/**
 * @file  cmdRPI.h
 * @author Matias Vidal Valladares - matias.vidal@ing.uchile.cl
 * @date 2022
 * @copyright GNU GPL v3
 *
 * This header has definitions of commands related to the Raspberry Pi driver features.
 */

#ifndef CMD_RPI_H
#define CMD_RPI_H

#include "suchai/config.h"

#include "suchai/repoCommand.h"
#include "suchai/repoData.h"

/**
 * Register the Raspberry Pi commands
 */
void cmd_rpi_init(void);

/**
 * Sends all the available commands to a given node.
 *
 * @param fmt Str. Parameters format "%u"
 * @param params Str. Parameters as string <node>
 * @param nparams Int. Number of parameters 1
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_ask_for_help(char *fmt, char *params, int nparams);

/**
 * Checks wether the camera is enabled or not and saves it as a status variable.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_camera(char *fmt, char *params, int nparams);

/*
 * Checks which are the missing frames and send their names to a node.
 *
 * @param fmt Str. Parameters format "%u %u"
 * @param params Str. Parameters as string <file_id, node>.
 *                    Ex: "rpi_check_frames 8 10"
 * @param nparams Int. Number of parameters 2
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_frames(char *fmt, char *params, int nparams);

/**
 * Checks wether the I2C is enabled or not and saves it as a status variable.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_i2c(char *fmt, char *params, int nparams);

/**
 * Checks wether the LEDs are enabled or not and saves it as a status variable.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_leds(char *fmt, char *params, int nparams);

/**
 * Checks wether the serial is enabled or not and saves it as a status variable.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_serial(char *fmt, char *params, int nparams);

/**
 * Checks wether the spi is enabled or not and saves it as a status variable.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_spi(char *fmt, char *params, int nparams);

/**
 * Checks the status variables of the Raspberry Pi.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_status(char *fmt, char *params, int nparams);

/**
 * Creates a delay of "delta" milliseconds.
 *
 * @param fmt Str. Parameters format "%u"
 * @param params Str. Parameters as string: <delta>
 * @param nparams Int. Number of parameters 1
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_delay(char *fmt, char *params, int nparams);

/**
 * Disables the Raspberry Pi's camera.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_camera(char *fmt, char *params, int nparams);

/**
 * Disables the bluetooth of the Raspberry Pi to save power.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_bluetooth(char *fmt, char *params, int nparams);

/**
 * Disables the Raspberry Pi's HDMI.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_hdmi(char *fmt, char *params, int nparams);

/* Disables the Raspberry Pi's I2C.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_i2c(char *fmt, char *params, int nparams);


/* Disables the Raspberry Pi's LED.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_led(char *fmt, char *params, int nparams);

/* Disables the Raspberry Pi's SPI.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_spi(char *fmt, char *params, int nparams);

/*
 * Disables the Raspberry Pi's wifi.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_wifi(char *fmt, char *params, int nparams);

/**
 * Enables the Raspberry Pi's camera.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_camera(char *fmt, char *params, int nparams);

/*
 * Re-enables the Raspberry Pi's bluetooth.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_bluetooth(char *fmt, char *params, int nparams);

/*
 * Re-enables the Raspberry Pi's HDMI.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_hdmi(char *fmt, char *params, int nparams);

/* Enables the Raspberry Pi's I2C.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_i2c(char *fmt, char *params, int nparams);

/*
 * Re-enables the Raspberry Pi's LED.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_led(char *fmt, char *params, int nparams);

/* Enables the Raspberry Pi's SPI.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_spi(char *fmt, char *params, int nparams);

/*
 * Re-enables the Raspberry Pi's wifi.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_wifi(char *fmt, char *params, int nparams);

/*
 * Reads the commands from a file and executes them from top to bottom.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string <path2fp>.
 *                    Ex: "flight_plan.txt"
 * @param nparams Int. Number of parameters 1
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_fp(char *fmt, char *params, int nparams);

/*
 * Sends pings from 0 to last_node and sends the resut to send_to.
 *
 * @param fmt Str. Parameters format "%u %u"
 * @param params Str. Parameters as string <send_to, last_node>.
 *                    Ex: "10 17"
 * @param nparams Int. Number of parameters 2
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_node_detect(char *fmt, char *params, int nparams);

/*
 * Records a video and saves it using the date and time as the name.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_record(char *fmt, char *params, int nparams);

/*
 * Execute a system call and prints its output.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string <node, rpi_sys_call>.
 *                    Ex: "2 df -h"
 * @param nparams Int. Number of parameters 2
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_system(char *fmt, char *params, int nparams);

/*
 * Takes a picture and saves it using the date and time as the name.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_take_picture(char *fmt, char *params, int nparams);

/*
 * Sends the output of Linux into an S band transceiver.
 *
 * @param fmt Str. Parameters format "%u %s"
 * @param params Str. Parameters as string "<node>, <rpy_sys_call>"
 * @param nparams Int. Number of parameters 2
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_tequila(char *fmt, char *params, int nparams);

/*
 * Prints all the avaliable commands for this payload.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_help(char *fmt, char *params, int nparams);

#endif /* CMD_RPI_H */
