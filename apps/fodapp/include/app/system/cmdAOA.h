/**
 * @file  cmdAOA.h
 * @author Matias Vidal Valladares - matias.vidal@ing.uchile.cl
 * @date 2022
 * @copyright GNU GPL v3
 *
 * This header has definitions of commands related to the Femto-satellite Orbital Deployer driver
 * (FOD) features.
 */

#ifndef CMD_AOA_H
#define CMD_AOA_H

#include "suchai/config.h"

#include "suchai/repoCommand.h"
#include "suchai/repoData.h"

#ifdef RPI
#include "bcm2835.h"
#endif

#define AOA_CS 16
#define AOA_IRQ_PIN  18
#define AOA_POWER_PIN 26

/**
 * Register the angle of arrival commands
 */
void cmd_aoa_init(void);

/* Reads the four channels of the ADC ADC084S051 which correspond to two
 * pairs of magnitude and phase of the four-antenna-array.
 * This data is saved as payload data in a struct.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int aoa_get_data(char *fmt, char *params, int nparams);

/*
 * Prints the current version of the AOA's software.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int aoa_get_version(char *fmt, char *params, int nparams);

/*
 * Powers on or off the AOA.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string: <power_on>. Ex: "1"
 * @param nparams Int. Number of parameters 1
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int aoa_pwr(char *fmt, char *params, int nparams);

/*
 * Restores the default values.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int aoa_reset(char *fmt, char *params, int nparams);

/*
 * Prints all the avaliable commands for this payload.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int aoa_help(char *fmt, char *params, int nparams);

#endif /* CMD_AOA_H */
