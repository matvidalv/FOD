#!/usr/bin/env bash
sudo apt-get update
sudo apt-get install gcc make cmake pkg-config libzmq3-dev libsqlite3-dev libpq-dev python libcap2 libcap-dev bc
cd ../RF24/
sudo make install
cd examples_linux/
sudo -u "$(logname)" cp ../../FOD/RF24/Makefile Makefile
sudo -u "$(logname)" cp ../../FOD/RF24/struct_test.cpp struct_test.cpp
sudo -u "$(logname)" cp ../../FOD/RF24/requestCarrierWave.cpp requestCarrierWave.cpp
sudo -u "$(logname)" make
cd ../../
sudo -u "$(logname)" tar zxvf bcm2835-1.70.tar.gz
rm bcm2835-1.70.tar.gz
cd bcm2835-1.70
sudo -u "$(logname)" ./configure
sudo -u "$(logname)" make
sudo make check
sudo make install
cd ../suchai-flight-software/
sudo -u "$(logname)" cp -r ../FOD/apps/fodapp apps/
sudo -u "$(logname)" cp ../bcm2835-1.70/src/bcm2835.c apps/fodapp/src/system/
