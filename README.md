# Femto-satellite Orbital Deployer (FOD)

This repository contains the PCB design and the source code of a femto-satellite orbital deployer or FOD. This device works by cutting a fishing line with Nichrome wire. The fishing line is normally pressing a hinge lever. When the line is cut, the hinge lever is released and the Raspberry Pi can detect the release status.

The PCB has a transceiver (NRF24L01) to communicate with the femto-satellite once it is released. There is also a LED showing the release status, and some mounting holes for a Raspberry Pi camera. The energy and the comunication with the OBC is done through a molex cable with a 12-pin picoblade connector.

This new version is designed for CubeSats of 2U or more. It is intended to be used as a type A interstage panel, so that it leaves more space inside the CubeSat for other payloads.

Current version: 4.220218

![](img/inside_outside.png)

### Supported software:

* KiCAD 5.99.0

### Working with the Raspberry Pi (RPI)

First download the following files and repositories:

```shell
wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.70.tar.gz
git clone -b feature/framework https://gitlab.com/spel-uchile/suchai-flight-software
git clone https://gitlab.com/matvidalv/FOD
git clone https://github.com/tmrh20/RF24
```

Now, for the installation run the script init.sh:

```shell
cd FOD/
sudo sh init.sh
```

Building for the Raspberry Pi is straight forward and very similar to any Linux.
Just connect to the RPi using SSH. Then, set proper build variables and build with
the following steps:

```shell
cmake -B build -DAPP=fodapp -DSCH_ARCH=RPI -DSCH_ST_MODE=SQLITE -DSCH_COMM_NODE=7 && cmake --build build
./build/apps/fodapp/suchai-app
```

### Supported hardware:

* Single board computer: Raspberry Pi Zero or 3B+
* Transceiver: NRF24L01
* Hinge lever: AV4524
* Mosfet: PMV16XNR

### Contact:

* Name: Matías Vidal Valladares
* e-mail: matias.vidal.v@gmail.com
